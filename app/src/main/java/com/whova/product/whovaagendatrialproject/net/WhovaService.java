package com.whova.product.whovaagendatrialproject.net;

import com.whova.product.whovaagendatrialproject.json.Example;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Tony Robalik (@AutonomousApps) on 7/30/15.
 */
public interface WhovaService {
    String ENDPOINT = "http://whova.com";

    @GET("/odesk/sched/detail/")
    void getExample(@Query("id") String id, Callback<Example> callback);

    @POST("/odesk/sched/action/")
    void postLike(@Query("action") String action, @Query("id") String id, Callback<Example> callback);
}