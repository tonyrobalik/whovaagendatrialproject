package com.whova.product.whovaagendatrialproject.json;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Detail {

    @Expose
    private List<Comment> comment = new ArrayList<Comment>();
    @Expose
    private String loc;
    @Expose
    private List<Speaker> speakers = new ArrayList<Speaker>();
    @Expose
    private String end;
    @Expose
    private List<Like> like = new ArrayList<Like>();
    @Expose
    private String title;
    @Expose
    private List<Checkin> checkin = new ArrayList<Checkin>();
    @Expose
    private String start;
    @Expose
    private String desc;

    /**
     * @return The comment
     */
    public List<Comment> getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }

    /**
     * @return The loc
     */
    public String getLoc() {
        return loc;
    }

    /**
     * @param loc The loc
     */
    public void setLoc(String loc) {
        this.loc = loc;
    }

    /**
     * @return The speakers
     */
    public List<Speaker> getSpeakers() {
        return speakers;
    }

    /**
     * @param speakers The speakers
     */
    public void setSpeakers(List<Speaker> speakers) {
        this.speakers = speakers;
    }

    /**
     * @return The end
     */
    public String getEnd() {
        return end;
    }

    /**
     * @param end The end
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * @return The like
     */
    public List<Like> getLike() {
        return like;
    }

    /**
     * @param like The like
     */
    public void setLike(List<Like> like) {
        this.like = like;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The checkin
     */
    public List<Checkin> getCheckin() {
        return checkin;
    }

    /**
     * @param checkin The checkin
     */
    public void setCheckin(List<Checkin> checkin) {
        this.checkin = checkin;
    }

    /**
     * @return The start
     */
    public String getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }
}