package com.whova.product.whovaagendatrialproject.json;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Speaker {

    @Expose
    private String loc;
    @Expose
    private String aff;
    @Expose
    private String pic;
    @Expose
    private String name;
    @Expose
    private String title;

    /**
     * @return The loc
     */
    public String getLoc() {
        return loc;
    }

    /**
     * @param loc The loc
     */
    public void setLoc(String loc) {
        this.loc = loc;
    }

    /**
     * @return The aff
     */
    public String getAff() {
        return aff;
    }

    /**
     * @param aff The aff
     */
    public void setAff(String aff) {
        this.aff = aff;
    }

    /**
     * @return The pic
     */
    public String getPic() {
        return pic;
    }

    /**
     * @param pic The pic
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }
}