package com.whova.product.whovaagendatrialproject;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.whova.product.whovaagendatrialproject.json.Checkin;
import com.whova.product.whovaagendatrialproject.json.Comment;
import com.whova.product.whovaagendatrialproject.json.Detail;
import com.whova.product.whovaagendatrialproject.json.Example;
import com.whova.product.whovaagendatrialproject.json.Speaker;
import com.whova.product.whovaagendatrialproject.net.WhovaService;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SessionDetailFragment.OnFragmentInteractionListener} interface to handle interaction
 * events. Use the {@link SessionDetailFragment#newInstance} factory method to create an instance of
 * this fragment.
 */
public final class SessionDetailFragment extends Fragment {

    public interface OnFragmentInteractionListener {
        // TODO implement
    }

    // Callback to Activity
    private OnFragmentInteractionListener mListener;

    // Parameterization of fragment
    private static final String ARG_1 = "arg_1";
    private String mParam1;

    // Model
    private Detail mDetail = null;
    private boolean mLike = false;

    // Widgets
    private TextView mTextTitle, mTextLocation, mTextTime, mTextOverview,
            mTextCheckIn, mTextLike, mTextComment;
    private ImageView mImgLike;
    private ViewGroup mContainer;

    // Whova API
    private static final String ID = "12";
    private static final String ACTION = "like";
    private WhovaService mWhovaService;

    public static SessionDetailFragment newInstance(@NonNull String param1) {
        SessionDetailFragment fragment = new SessionDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_1);
        }

        // GET
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WhovaService.ENDPOINT)
                .build();
        mWhovaService = restAdapter.create(WhovaService.class);
        mWhovaService.getExample(ID, new Callback<Example>() {
            @Override
            public void success(Example example, Response response) {
                // This object has all the actual data
                Log.d("Retrofit success", "Result=" + example.getResult().getResult());
                mDetail = example.getResult().getDetail();
                bindModel();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Retrofit failure", "error=" + error);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Remember for later
        mContainer = container;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_session_detail, container, false);

        // Get reference to widgets
        mTextTitle = (TextView) view.findViewById(R.id.text_title);
        mTextLocation = (TextView) view.findViewById(R.id.text_location);
        mTextTime = (TextView) view.findViewById(R.id.text_time);
        mTextOverview = (TextView) view.findViewById(R.id.text_overview);
        mTextCheckIn = (TextView) view.findViewById(R.id.text_check_in);
        mTextLike = (TextView) view.findViewById(R.id.text_like);
        mTextComment = (TextView) view.findViewById(R.id.text_comment);

        mImgLike = (ImageView) view.findViewById(R.id.img_like);

        // Add to agenda
        view.findViewById(R.id.card_add_agenda).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Adding to my agenda", Toast.LENGTH_SHORT).show();
            }
        });


        // Check-in
        view.findViewById(R.id.card_check_in).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Checking in", Toast.LENGTH_SHORT).show();
            }
        });

        // Like
        view.findViewById(R.id.card_like).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mWhovaService.postLike(ACTION, ID, new Callback<Example>() {
                    @Override
                    public void success(Example example, Response response) {
                        if (example.getResult().getResult().equals("success")) {
                            if (mLike) {
                                mLike = !mLike;
                                mImgLike.setImageResource(R.drawable.heart_empty_icon);
                            } else {
                                mLike = !mLike;
                                mImgLike.setImageResource(R.drawable.heart_solid_icon);
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("POST", "RetrofitError: " + error.getLocalizedMessage());
                    }
                });
            }
        });

        // Comment
        view.findViewById(R.id.card_comment).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Commenting", Toast.LENGTH_SHORT).show();
            }
        });


        // Bind model to view
        bindModel();

        return view;
    }

    // TODO race conditions?
    private void bindModel() {
        if (mDetail == null) return;
        if (getView() == null) return;

        // Basic information
        mTextTitle.setText(mDetail.getTitle());
        mTextLocation.setText(mDetail.getLoc());
        mTextTime.setText(mDetail.getStart() + " - " + mDetail.getEnd());
        mTextOverview.setText(mDetail.getDesc());

        // Update with counts
        String text = getString(R.string.text_checkin) +
                String.format(" (%d)", mDetail.getCheckin().size());
        mTextCheckIn.setText(text);
        text = getString(R.string.text_like) +
                String.format(" (%d)", mDetail.getLike().size());
        mTextLike.setText(text);
        text = getString(R.string.text_comment) +
                String.format(" (%d)", mDetail.getComment().size());
        mTextComment.setText(text);

        // Speaker information
        Speaker speaker = mDetail.getSpeakers().get(0); // TODO don't assume this in production code
        ViewGroup speakerContainer = (ViewGroup) getView().findViewById(R.id.layout_speaker);
        ImageView speakerPic = (ImageView) speakerContainer.findViewById(R.id.img_photo);
        new DownloadImageTask(speakerPic).execute(speaker.getPic());
        ((TextView) speakerContainer.findViewById(R.id.text_name)).setText(speaker.getName());
        ((TextView) speakerContainer.findViewById(R.id.text_title)).setText(speaker.getTitle());
        ((TextView) speakerContainer.findViewById(R.id.text_affiliation)).setText(speaker.getAff());
        ((TextView) speakerContainer.findViewById(R.id.text_location)).setText(speaker.getLoc());

        // Get LayoutInflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Check-ins
        ViewGroup checkinContainer = (ViewGroup) getView().findViewById(R.id.list_checked_in);
        List<Checkin> checkins = mDetail.getCheckin();
        // Set title count
        ((TextView) getView().findViewById(R.id.text_checked_in_title))
                .setText(getString(R.string.text_checked_in) + String.format(" (%d)", checkins.size()));
        // Add check-ins
        for (Checkin checkin : checkins) {
            ViewGroup checkinView = (ViewGroup) inflater.inflate(R.layout.list_layout, mContainer, false);
            ImageView checkinPic = (ImageView) checkinView.findViewById(R.id.img_photo);
            new DownloadImageTask(checkinPic).execute(checkin.getPic());
            ((TextView) checkinView.findViewById(R.id.text_name)).setText(checkin.getName());
            ((TextView) checkinView.findViewById(R.id.text_title)).setText(checkin.getTitle());
            ((TextView) checkinView.findViewById(R.id.text_affiliation)).setText(checkin.getAff());
            ((TextView) checkinView.findViewById(R.id.text_location)).setText(checkin.getLoc());
            checkinContainer.addView(checkinView);
        }

        // Comments
        ViewGroup commentContainer = (ViewGroup) getView().findViewById(R.id.list_comments);
        List<Comment> comments = mDetail.getComment();
        // Set title count
        ((TextView) getView().findViewById(R.id.text_comments_title))
                .setText(getString(R.string.text_comments) + String.format(" (%d)", comments.size()));
        // Add comments
        for (Comment comment : comments) {
            ViewGroup commentView = (ViewGroup) inflater.inflate(R.layout.comment_layout, mContainer, false);
            ImageView commentPic = (ImageView) commentView.findViewById(R.id.img_photo);
            new DownloadImageTask(commentPic).execute(comment.getPic());
            ((TextView) commentView.findViewById(R.id.text_name)).setText(comment.getName());
            ((TextView) commentView.findViewById(R.id.text_time)).setText(comment.getTime());
            ((TextView) commentView.findViewById(R.id.text_comment)).setText(comment.getComment());
            commentContainer.addView(commentView);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private final class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private final ImageView mImageView;

        private DownloadImageTask(@NonNull ImageView imageView) {
            mImageView = imageView;
        }

        @Nullable
        @Override
        protected Bitmap doInBackground(@NonNull String... urls) {
            String url = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = null;
                try {
                    in = new URL(url).openStream();
                    // TODO deal with rectangular bitmaps
                    bitmap = roundBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeStream(in),
                            mImageView.getWidth(), mImageView.getHeight(), true));
                } finally {
                    if (in != null) in.close();
                }
            } catch (MalformedURLException e) {
                Log.e("DownloadImageTask", "Malformed URL: " + e.getLocalizedMessage());
            } catch (IOException e) {
                Log.e("DownloadImageTask", "IOException: " + e.getLocalizedMessage());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(@Nullable Bitmap result) {
            mImageView.setImageBitmap(result);
        }

        private Bitmap roundBitmap(Bitmap bitmap) {
            BitmapShader shader;
            shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(shader);

            RectF rect = new RectF(0.0f, 0.0f, mImageView.getWidth(), mImageView.getHeight());
            Bitmap b = Bitmap.createBitmap(mImageView.getWidth(), mImageView.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(b);
            canvas.drawRoundRect(rect, mImageView.getWidth() / 2, mImageView.getWidth() / 2, paint);

            return b;
        }
    }
}