package com.whova.product.whovaagendatrialproject.json;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Result {

    @Expose
    private String result;
    @Expose
    private Detail detail;

    /**
     * @return The result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return The detail
     */
    public Detail getDetail() {
        return detail;
    }

    /**
     * @param detail The detail
     */
    public void setDetail(Detail detail) {
        this.detail = detail;
    }
}