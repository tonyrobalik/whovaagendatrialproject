package com.whova.product.whovaagendatrialproject.json;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Comment {

    @Expose
    private String comment;
    @Expose
    private String pic;
    @Expose
    private String name;
    @Expose
    private String time;

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The pic
     */
    public String getPic() {
        return pic;
    }

    /**
     * @param pic The pic
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    public void setTime(String time) {
        this.time = time;
    }
}